val joobyVersion by extra("2.7.3") // https://github.com/jooby-project/jooby/releases

plugins {
  val kotlinVersion = "1.3.70" // https://blog.jetbrains.com/kotlin/category/releases/
  java
  `java-library`
  jacoco
  id("org.jetbrains.kotlin.jvm") version kotlinVersion
  id("org.jetbrains.kotlin.kapt") version kotlinVersion
  id("org.ec4j.editorconfig") version "0.0.3" // https://plugins.gradle.org/plugin/org.ec4j.editorconfig
  id("org.owasp.dependencycheck") version "5.3.1" // https://plugins.gradle.org/plugin/org.owasp.dependencycheck
  id("net.researchgate.release") version "2.8.1" // https://plugins.gradle.org/plugin/net.researchgate.release
}

allprojects {

  apply(plugin = "java-library")

  repositories {
    mavenCentral()
    jcenter()
  }
}

subprojects {
  group = "sandbox"
  version = project.version

  apply(plugin = "jacoco")
  apply(plugin = "org.jetbrains.kotlin.jvm")
  apply(plugin = "org.jetbrains.kotlin.kapt")

  dependencies {
    implementation(kotlin("stdlib"))
    implementation(kotlin("reflect"))
    testImplementation(kotlin("test"))
    testImplementation(kotlin("test-junit"))

    implementation(platform("io.jooby:jooby-bom:${joobyVersion}"))
    implementation("io.jooby:jooby")
    implementation("ch.qos.logback:logback-classic")
    testImplementation("io.jooby:jooby-test")
    testImplementation("org.assertj:assertj-core:3.15.0")
  }

  tasks {

    withType<JavaCompile> {
      sourceCompatibility = JavaVersion.VERSION_11.toString()
      targetCompatibility = JavaVersion.VERSION_11.toString()
    }

    withType<org.jetbrains.kotlin.gradle.tasks.KotlinCompile>().configureEach {
      kotlinOptions {
        jvmTarget = JavaVersion.VERSION_1_8.toString()
        freeCompilerArgs = listOf("-progressive")
      }
    }

    test {
      useJUnitPlatform()
    }
  }
}

tasks {

  defaultTasks("clean", "editorconfigCheck", "dependencyCheckAggregate", "assemble")
}

editorconfig {
  excludes = listOf(
    "gradle**",
    "**/build/**",
    "**/node_modules/**",
    "**/src/main/resources/static/**"
  )
}

dependencyCheck {
  failBuildOnCVSS = 8.0f
  suppressionFile = "suppression.xml"
}

release {
  tagTemplate = "v\$version"
}
