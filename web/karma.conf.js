module.exports = function(config) {
  config.set({
    basePath: '.',
    frameworks: ['jasmine'],
    plugins: [
      'karma-jasmine',
      'karma-chrome-launcher',
      'karma-firefox-launcher',
      'karma-webpack',
      'karma-coverage',
      'karma-junit-reporter'
    ],
    files: [
      'src/main/js/*.js',
      'src/test/js/*.js'
    ],
    exclude: [],
    preprocessors: {
      'src/main/js/*.js': ['webpack', 'coverage']
    },
    webpack: {
      mode: 'development',
    },
    reporters: ['progress', 'coverage', 'junit'],
    coverageReporter: {
      type: 'lcovonly',
      dir: 'build/js/coverage',
      subdir: '.'
    },
    junitReporter: {
      outputDir: 'build/js/surefire-reports'
    },
    browsers: [
      'ChromeHeadless',
      'FirefoxHeadless'
    ],
    singleRun: true
  })
}
