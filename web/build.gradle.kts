val joobyVersion = project.rootProject.ext["joobyVersion"]

base {
  archivesBaseName = "sandbox-web"
}

plugins {
  id("com.github.node-gradle.node") version "2.2.3" // https://plugins.gradle.org/plugin/com.github.node-gradle.node
}

dependencies {
  implementation(project(":core"))
  testImplementation(project(":core", "testRuntime"))

  implementation("io.jooby:jooby-netty")
  kapt("io.jooby:jooby-apt:${joobyVersion}")
}

tasks {

  task("npmCleanInstall", com.moowork.gradle.node.npm.NpmTask::class) {
    setArgs(listOf("clean-install"))
  }

  task("buildFrontend", com.moowork.gradle.node.npm.NpmTask::class) {
    dependsOn("npmCleanInstall")
    setArgs(listOf("run", "build"))
  }

  task("testFrontend", com.moowork.gradle.node.npm.NpmTask::class) {
    setArgs(listOf("test"))
  }

  build {
    dependsOn("buildFrontend")
  }

  test {
    dependsOn("testFrontend")
  }

  jar {
    archiveFileName.set("${archiveBaseName.get()}.${archiveExtension.get()}")
  }
}

node {
  // https://nodejs.org/ja/download/releases/
  version = "12.16.1"
  npmVersion = "6.13.4"
  download = true
}
