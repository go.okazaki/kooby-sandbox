val joobyVersion = project.rootProject.ext["joobyVersion"]

base {
  archivesBaseName = "sandbox-api"
}

plugins {
  id("org.asciidoctor.jvm.convert") version "3.1.0" // https://plugins.gradle.org/plugin/org.asciidoctor.jvm.convert
}

var snippetsDir = file("${buildDir}/generated-snippets")

dependencies {
  implementation(project(":core"))
  testImplementation(project(":core", "testRuntime"))
}

tasks {

  jar {
    archiveFileName.set("${archiveBaseName.get()}.${archiveExtension.get()}")
  }

  test {
    finalizedBy("asciidoctor")
  }

  asciidoctor {
    attributes(
      mapOf(
        "snippets" to "${snippetsDir}"
      )
    )
  }
}
