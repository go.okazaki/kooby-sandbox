val joobyVersion = project.rootProject.ext["joobyVersion"]

plugins {
  id("io.ebean") version "12.1.10"
}

dependencies {
  api("io.jooby:jooby-gson")
  api("io.jooby:jooby-hikari")
  api("io.jooby:jooby-ebean")
  api("io.jooby:jooby-flyway")
  api("io.jooby:jooby-pac4j")
  api("io.ebean:ebean-querybean")
  implementation("org.mindrot:jbcrypt:0.4")
  annotationProcessor("io.ebean:querybean-generator")
  runtimeOnly("org.postgresql:postgresql:42.2.11")
  testImplementation("io.ebean:ebean-test")
}

tasks {

  val testJar = task("testJar", org.gradle.api.tasks.bundling.Jar::class) {
    archiveClassifier.set("tests")
    from(sourceSets.test.get().output)
  }

  artifacts {
    testRuntime(testJar)
  }

  jar {
    enabled = true
  }
}
